; ______        _         _                      _____                _          _____  _____ 
; | ___ \      (_)       | |                    /  ___|              | |        |  _  |/  ___|
; | |_/ / __ _  _  _ __  | |__    ___ __      __\ `--.  _ __    __ _ | | __ ___ | | | |\ `--. 
; |    / / _` || || '_ \ | '_ \  / _ \\ \ /\ / / `--. \| '_ \  / _` || |/ // _ \| | | | `--. \
; | |\ \| (_| || || | | || |_) || (_) |\ V  V / /\__/ /| | | || (_| ||   <|  __/\ \_/ //\__/ /
; \_| \_|\__,_||_||_| |_||_.__/  \___/  \_/\_/  \____/ |_| |_| \__,_||_|\_\\___| \___/ \____/ 
;                                                                                            
;
;           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
;                   Version 2, December 2004
; 
;Copyright (C) 2023 Palma3000 <palma3000@airmail.cc>
;
;Everyone is permitted to copy and distribute verbatim or modified
;copies of this license document, and changing it is allowed as long
;as the name is changed.
; 
;           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
;  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
;
; 0. You just DO WHAT THE FUCK YOU WANT TO.
;

format binary

include "struct.inc"

use16
org 0x0

STATE_STOP = 0x0
STATE_UP = 0x1
STATE_DOWN = 0x2
STATE_LEFT = 0x3
STATE_RIGHT = 0x4
STATE_GAMEOVER = 0x5
STATE_WIN = 0x6

KEY_UP = 0x4800
KEY_DOWN = 0x5000
KEY_LEFT = 0x4B00
KEY_RIGHT = 0x4D00
KEY_ESC = 0x011B

SNAKE_MAX_LENGTH = 0xF

SCREEN_WIDTH = 320
SCREEN_HEIGHT = 200

struct VARS 
  len rw 1
  coords rw SNAKE_MAX_LENGTH
  food rw 1
  state rb 1
  res rb 1
ends


entry_point:
    cld
    cmp     Byte [cs:0], 0xCD
    jnz     short .dos
    mov     ax, cs
    mov     ds, ax
    mov     es, ax
    cli
    mov     ss, ax
    mov     sp, ax
    sti
.dos:    
    mov     ax, 0x13
    int     0x10
    sub     sp, sizeof.VARS
    mov     bp, sp
reset:
    mov     di, bp
    xor     ax, ax
    inc     ax
    stosw
    mov     ax, 0x505
    stosw
    mov     Word [bp + VARS.food], 0x1010
    mov     Byte [bp + VARS.state], STATE_STOP
    xor     ax, ax
    call    draw
main_loop:
    mov     al, [bp + VARS.state]
    cmp     al, STATE_GAMEOVER
    jz      short .gameover
    cmp     al, STATE_WIN
    jz      short .win
    call    move
    mov     al, [bp + VARS.state]
    cmp     al, STATE_GAMEOVER
    jae     short main_loop
    mov     ah, 0x1
    int     0x16
    jz      short main_loop
.read_key:
    xor     ah, ah 
    int     0x16
    xchg    dx, ax
    call    @f
.key_table:
    dw KEY_UP,    (STATE_DOWN shl 0x8) + STATE_UP
    dw KEY_DOWN,  (STATE_UP shl 0x8) + STATE_DOWN
    dw KEY_LEFT,  (STATE_RIGHT shl 0x8) + STATE_LEFT
    dw KEY_RIGHT, (STATE_LEFT shl 0x8) + STATE_RIGHT
    dw KEY_ESC,   (0xFF shl 0x8) + STATE_GAMEOVER
    dw 0
@@:
    pop     si
    mov     bx, Word [bp]
.check_loop:
    lodsw
    test    ax, ax
    jz      short main_loop
    mov     di, ax
    lodsw
    cmp     di, dx
    jnz     short .check_loop
    dec     bx
    je      .set_state
    cmp     Byte [bp + VARS.state], ah
    je      short main_loop
.set_state:
    mov     Byte [bp + VARS.state], al
    jmp     short main_loop
.gameover:
    call    @f
.str_gameover db 'GAME OVER'
len_str_gameover = $-.str_gameover
@@:
    push    len_str_gameover
    jmp     short .exit
.win:
    call    @f
    .str_win      db 'U WIN'
    len_str_win = $-.str_win
@@:
    push    len_str_win
.exit:
    mov     ax, 0x3
    int     0x10
    mov     ax, 0x1300
    mov     bx, 0x2
    pop     cx
    pop     bp
    mov     dx, 0xA20
    int     0x10
    xor     ax, ax
    int     0x16
    xor     bx, bx
    cmp     Byte [cs:bx], 0xCD
    jz      .dos
    db      0x0ea, 0x00, 0x00, 0xff, 0xff
.dos:
    xor     ax, ax
    int     0x20


move:
    pusha
    mov     si, bp
    lodsw
    dec     ax
    shl     ax, 1
    add     si, ax
    mov     di, si
    lodsw
    xchg    cx, ax
    mov     al, Byte [bp + VARS.state]
    cmp     al, STATE_STOP
    jz      .down
    cmp     al, STATE_UP
    jnz     @f
    test    ch, ch
    je      short .gameover
    dec     ch
    jmp     short .process
@@:
    cmp     al, STATE_DOWN
    jnz     @f
    cmp     ch, SCREEN_HEIGHT/4 - 1
    jae     short .gameover
    inc     ch
    jmp     short .process
@@:
    cmp     al, STATE_LEFT
    jnz     @f
    test    cl, cl
    je      short .gameover
    dec     cl
    jmp     short .process
@@:
    cmp     al, STATE_RIGHT
    jnz     short .gameover
    cmp     cl, SCREEN_WIDTH/4 - 1
    jae     short .gameover
    inc     cl
.process:
    call    check_snake_block
    jz      short .gameover
    cmp     cx, Word [bp + VARS.food]
    jnz     short .not_food
    cmp     Word [bp], SNAKE_MAX_LENGTH
    jae     short .win
    inc     Word [bp]
    mov     Word [di+2], cx
    mov     ax, (SCREEN_WIDTH/4)-1
    call    get_random
    inc     ax
    mov     Byte [bp + VARS.food], al
    mov     ax, (SCREEN_HEIGHT/4)-1
    call    get_random
    inc     ax
    mov     Byte [bp + VARS.food + 1], al
    xor     ax, ax
    jmp     short .draw
.not_food:
    mov     si, bp
    lodsw
    mov     dx, ax
    push    Word [si]
.shift_loop:
    dec     dx
    jz      short .end_shift
    mov     ax, Word [si+0x2]
    mov     Word [si], ax
    lodsw
    jmp     short .shift_loop
.end_shift:
    mov     Word [si], cx
    pop     ax
.draw:
    call    draw
    jmp     short .down
.gameover:
    mov     al, STATE_GAMEOVER
    jmp     short .set_state
.win:
    mov     al, STATE_WIN
.set_state:
    mov     Byte [bp + VARS.state], al
.down:
    popa
    ret

    
get_random:
    push    dx
    push    cx
    push    ax
    xor     ax, ax
    int     0x1A
    pop     cx
    xchg    ax, dx
    xor     dx, dx
    div     cx
    xchg    ax, dx
    pop     cx
    pop     dx
    ret


draw_snake_rect:
    pusha
    mov     si, 320
    xor     dx, dx
    xchg    dl, ah
    xor     ah, ah
    shl     dx, 0x2
    shl     ax, 0x2
    push    ax
    xchg    dx, ax
    mul     si
    pop     dx
    add     ax, dx
    xchg    di, ax
    mov     dx, 0x4
.draw_line:
    push    di
    mov     cx, 0x4
    mov     al, bl
    rep     stosb
    pop     di
    add     di, si
    dec     dl
    jnz     short .draw_line
    popa
    ret


check_snake_block:
    pusha
    mov     di, bp
    mov     ax, cx
    mov     cx, Word [di]
    inc     di
    inc     di
    repne   scasw
    popa
    ret


draw:
    push    es
    pusha
    push    0xA000
    pop     es
    push    ax
    mov     dx, 3DAh
.l1:
    in      al,dx
    and     al,08h
    jnz     short .l1
.l2:
    in      al,dx
    and     al,08h
    jz      short .l2
    mov     si, bp
    lodsw
    mov     cx, ax
    mov     bx, cx
    inc     bx
.draw_snake_loop:
    lodsw
    dec     bx
    call    draw_snake_rect
    dec     cx
    jnz     short .draw_snake_loop
.draw_food:
    mov     ax, Word [bp + VARS.food]
    mov     bl, 0xA
    call    draw_snake_rect
    pop     ax
    test    ax, ax
    jz      @f
    xor     bx, bx
    call    draw_snake_rect
@@:
    xor     cx, cx
    mov     dx, 0x7000
    mov     ah, 0x86
    int     0x15
    popa
    pop     es
    ret
    
    
rb 0x200-2-$
dw 0xAA55
