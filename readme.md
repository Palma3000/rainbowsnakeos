# RainbowSnakeOS

![](screenshot.png)

RainbowSnakeOS, a 512 byte OS and snake video game written in 16-bit x86 assembly language (FASM). It can be run from boot sector and MS DOS as .com application.

[Download binary executable file "snake.com"](https://gitlab.com/Palma3000/rainbowsnakeos/uploads/58979083d2a18c65c132d6b4a01b92db/snake.com)

## License

```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004
 
Copyright (C) 2023 Palma3000 <palma3000@airmail.cc>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
```
